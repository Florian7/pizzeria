<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pizza")
 * @ORM\Entity(repositoryClass="App\Repository\PizzaRepository")
 */
class Pizza
{
    /**
     * @var int
     * @ORM\Column(name="id_pizza", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @var Collection
     * Many Pizzas have Many Ingredients.
     * @ORM\ManyToMany(targetEntity="App\Entity\IngredientPizza")
     * @ORM\JoinTable(name="ingredients_pizzas",
     *      joinColumns={@ORM\JoinColumn(name="pizza_id", referencedColumnName="id_pizza")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="ingredient_pizza_id", referencedColumnName="id_ingredient_pizza", unique=true)}
     *      )
     */
    private $quantiteIngredients;

    /**
     * @return float
     */
    //méthode permettant d'afficher le prix de la pizza sélécionnée
    public function getCout()
    {
        

        //initialisation de la variable à 0 pour pouvoir cumuler a chaque fois que l'on parcourt la boucle le prix total
        $prixTotal = 0;
        /**
         * @var IngredientPizza $IngredientPizza
         */
        
        //la boucle est parcouru afin d'afficher les informations des ingrédient de la pizza séléctionnée
        foreach($this->quantiteIngredients as $ingredientPizza){

            //on stock la quantité d'ingrédient grâce à la méthode getQuantite()
            $quantite=$ingredientPizza->getQuantite();

            //on convertit la quantite d'ingredient de g en kg grâce la méthode static convertirGrameEnKilo se trouvant dans la classe IngredientPizza
            $kilo=IngredientPizza::convertirGrammeEnKilo($quantite);

            //on stock l'ingrédient que l'on obtient de la classe IngredientPizza grâce à la méthode getIngredient()
            $ingredient=$ingredientPizza->getIngredient();

            //on obtient le coût de l'ingrédient stocké grâce à la méthode getCout() contenue dans la classe Ingredient
            $coutKilo=$ingredient->getCout();

            //on additionne le prix total avec le prix de l'ingrédient courant de l'ingrédient qui ets calculé en fonction de son poids et son prix/kg
            $prixTotal +=$kilo*$coutKilo;
        }
        
        //on retourne le prix total de tout les ingrédients de la pizza
        return $prixTotal;
    }
 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quantiteIngredients = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Pizza
     */
    public function setId(int $id): Pizza
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Pizza
     */
    public function setNom(string $nom): Pizza
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @param IngredientPizza $quantiteIngredients
     * @return Pizza
     */
    public function addQuantiteIngredients(IngredientPizza $quantiteIngredients): Pizza
    {
        $this->quantiteIngredients[] = $quantiteIngredients;

        return $this;
    }

    /**
     * @param IngredientPizza $quantiteIngredients
     */
    public function removeQuantiteIngredient(IngredientPizza $quantiteIngredients): void
    {
        $this->quantiteIngredients->removeElement($quantiteIngredients);
    }

    /**
     * @return Collection
     */
    public function getQuantiteIngredients(): Collection
    {
        return $this->quantiteIngredients;
    }
}
