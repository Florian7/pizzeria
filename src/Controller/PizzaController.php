<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\IngredientPizza;
use App\Entity\Pizza;
use App\Service\Dao\PizzaDao;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PizzaController
 * @package App\Controller
 */
class PizzaController extends AbstractController
{
    /**
     * @param PizzaDao $pizzaDao
     * @Route("/pizzas")
     * @return Response
     */
    public function listeAction(PizzaDao $pizzaDao): Response
    {
        // récupération des différentes pizzas
        $pizzas = $pizzaDao->getAllPizzas();

        return $this->render("Pizza/liste.html.twig", [
            "pizzas" => $pizzas,
        ]);
    }

    /**
     * @param int $pizzaId
     * @Route(
     *     "/pizzas/detail-{pizzaId}",
     *     requirements={"pizzaId": "\d+"}
     * )
     * @return Response
     */
    public function detailAction(int $pizzaId, PizzaDao $pizzaDao): Response
    {
        //on stock le détail de la pizza séléctionnée : son id, son nom et la collection de la quantite d'ingrédient
        $pizza = $pizzaDao->getDetailPizza($pizzaId);
        
        //vue qui permet d'afficher le prix de fabrication de la pizza séléctionnée grâce à la méthode getCout()
        //elle permet aussi d'afficher la liste des ingrédient que contient la pizza séléctionnée
        return $this->render("Pizza/detail.html.twig", array('pizza' => $pizza));
    }
}
