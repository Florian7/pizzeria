<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Pizzeria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class PizzeriaRepository
 * @package App\Repository
 */
class PizzeriaRepository extends ServiceEntityRepository
{
    /**
     * PizzeriaRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pizzeria::class);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        // exécution de la requête
        return parent::findAll();
    }

    /**
     * @param int $pizzeriaId
     * @return Pizzeria
     */
    public function findCartePizzeria($pizzeriaId): Pizzeria
    {
        // test si l'id de la pizzeria est bien un nombre supérieur à 0
        if (!is_numeric($pizzeriaId) || $pizzeriaId <= 0){
            throw new \Exception("Impossible d'obtenir le détail de la pizzeria ({$pizzeriaId}).");
        }
        
        //création du query builder avec l'alias pi pour pizzeria
        $qb = $this->createQueryBuilder("pi");

        //création de la requête
        $qb
            ->addSelect(["pizza", "qte", "ing"])
            ->innerJoin("pi.pizzas", "pizza")
            ->innerJoin("pizza.quantiteIngredients", "qte")
            ->innerJoin("qte.ingredient", "ing")
            ->where("pi.id = :idPizzeria")
            ->setParameter("idPizzeria", $pizzeriaId)
        ;

        //exécution de la requête
        return $qb->getQuery()->getSingleResult();
    }
}
